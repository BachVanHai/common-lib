package nth.lib.common.status;

public enum ApprovalStatus {
  PREPARING("PREPARING", "Is preparing contract to request for approval"),
  SUBMITTED("SUBMITTED", "Contract is submitted for approval"),
  APPROVED("APPROVED", "Is approved by this step in workflow"),
  REJECTED("REJECTED", "Is rejected by this step in workflow"),
  FAILED("FAILED", "Is failed when approval process"),
  TIMEOUT("TIMEOUT", "Timeout when approval to next step");

  private String type;
  private String name;

  ApprovalStatus(String type, String name) {
    this.type = type;
    this.name = name;
  }

  public String getType() {
    return this.type;
  }

  public String getName() {
    return this.name;
  }
}
