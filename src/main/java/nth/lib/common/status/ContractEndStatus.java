package nth.lib.common.status;

public enum ContractEndStatus {
  PROCESSING("PROCESSING", "Contract is in-processing"),
  COMPLETED("COMPLETED", "Contract is completed by all steps in workflow"),
  REJECTED("REJECTED", "Contract is rejected by any step in workflow"),
  CANCELLED("CANCELLED", "Contract is cancelled by any step in workflow");

  private String type;
  private String name;

  ContractEndStatus(String type, String name) {
    this.type = type;
    this.name = name;
  }

  public String getType() {
    return this.type;
  }

  public String getName() {
    return this.name;
  }
}
