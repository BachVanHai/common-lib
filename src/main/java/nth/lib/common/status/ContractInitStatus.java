package nth.lib.common.status;

public enum ContractInitStatus {
  INIT("INIT", "Init contract"),
  DRAFT("DRAFT", "Draft an contract"),
  SUBMITTED("SUBMITTED", "Contract is submitted to system or agency account"),
  FAILED("FAILED", "Is failed when submitting to next step"),
  TIMEOUT("TIMEOUT", "Timeout when submitting to next step");

  private String type;
  private String name;

  ContractInitStatus(String type, String name) {
    this.type = type;
    this.name = name;
  }

  public String getType() {
    return this.type;
  }

  public String getName() {
    return this.name;
  }
}
