package nth.lib.common.type;

import org.apache.commons.lang.ArrayUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface Constants {

    String EMPTY = "";
    String ZERO_STR = "0";
    String DOT = ".";
    String VN_INTERNATIONAL_PHONE_CODE = "84";
    String NUMBER_FORMAT_PATTERN = "###,###";

    String NON_DIGIT_PATTERN = "\\D";

    MathContext DEFAULT_MATH_CONTEXT = new MathContext(3);
    BigDecimal MINIMUM_BALANCE = BigDecimal.ZERO;

    String YES = "Y";
    String NO = "N";

    String COMMA = ",";
    String HYPHEN = "-";
    String MINUS = "-";
    String UNDERSCORE = "_";

    int CUSTOMER_AUTH_METHOD_DISABLE_STATUS = 1;
    int CUSTOMER_AUTH_METHOD_ENABLE_STATUS = 0;

    List<String> ROLE_PARTNERS = new ArrayList(Arrays.asList(ArrayUtils.addAll(UserRole.getPartnerRoles(), new String[]{
            UserRole.DIVAYL1.getValue(), UserRole.DIVAYL2.getValue(), UserRole.DIVAYL3.getValue(), UserRole.DIVAYL4.getValue(), UserRole.DIVAYL5.getValue()})));

    String DIVAY_ORDER_PREFIX = "DIVAY-IO-BSH-";
    String FICO_SALE_MANAGER = "FS14625";

    String ERROR_LOG_KAFKA_TOPIC = "ERROR_LOG_KAFKA_TOPIC";




}
