package nth.lib.common.type;

import java.util.List;

public enum UserRole {
    ADMIN("AD.IO"), KD("KD.IO"), HTKD("HT.IO"), KT("KT.IO"), VH("VH.IO"),
    DTL1("L1.DT"), DTL2("L2.DT"), DTL3("L3.DT"), DTL4("L4.DT"), DTL5("L5.DT"),
    DIVAYL1("DIVAY.L1"), DIVAYL2("DIVAY.L2"), DIVAYL3("DIVAY.L3"), DIVAYL4("DIVAY.L4"), DIVAYL5("DIVAY.L5"),
    DTLX("LX.DT"), BH("BH"), BTBH("BTBH"), HTDT("HT.DT");

    private final String value;

    UserRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String[] getPartnerRoles() {
        int MAX_ROLE = 30;
        String[] partnerRoles = new String[MAX_ROLE];
        for (int i = 0; i < MAX_ROLE; i++) {
            partnerRoles[i] = String.format("L%s.DT",i + 1);
        }
        return partnerRoles;
    }
}
