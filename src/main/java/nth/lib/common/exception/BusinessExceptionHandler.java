package nth.lib.common.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
@Order(value = PriorityOrdered.HIGHEST_PRECEDENCE)
public class BusinessExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessExceptionHandler.class);

    @ExceptionHandler(value = {Throwable.class})
    protected ResponseEntity<ErrorResponse> handleBusinessException(Throwable ex) {
        LOGGER.error(getClass().getSimpleName(), ex);

        LOGGER.warn(String.format("%s ---> {errorType: %s, errorMessage: %s}",
                getClass().getSimpleName(),
                ex.getClass().getName(),
                ex.getMessage()));

        HttpHeaders headers = new HttpHeaders();

        if (ex instanceof BusinessException) {
            return new ResponseEntity<>(((BusinessException) ex).getResponse(), headers, HttpStatus.BAD_REQUEST);
        }

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        errorResponse.setErrorCode("500");
        errorResponse.setErrorDesc(ex.getMessage());
        return new ResponseEntity<>(errorResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
