package nth.lib.common.exception;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ErrorHelper {

    private static MessageSource messageSource;

    public static void setMessageSource(MessageSource messageSource) {
        ErrorHelper.messageSource = messageSource;
    }

    public static ErrorResponse buildErrorObject(String errorKey) {
        return ErrorHelper.buildErrorObject(errorKey, null);
    }


    public static ErrorResponse buildErrorObject(String errorKey, Object[] params) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(errorKey);
        errorResponse.setMessage(messageSource.getMessage("error." + errorKey, params, LocaleContextHolder.getLocale()));
        return  errorResponse;
    }
}
