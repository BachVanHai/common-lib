package nth.lib.common.exception;

import lombok.Getter;
import lombok.Setter;

public class BusinessException extends RuntimeException {

    @Getter
    @Setter
    private ErrorResponse response;


    public BusinessException(String errorKey) {
        super();
        setResponse(ErrorHelper.buildErrorObject(errorKey, null));
    }

    public BusinessException(String errorMessage, boolean notErrorKey) {
        super();
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage(errorMessage);
        setResponse(errorResponse);
    }

    public BusinessException(String errorKey, Object[] params) {
        super();
        setResponse(ErrorHelper.buildErrorObject(errorKey, params));
    }


}
