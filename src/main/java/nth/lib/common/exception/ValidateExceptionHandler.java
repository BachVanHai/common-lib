package nth.lib.common.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import nth.lib.common.utils.ExceptionHandlerUtils;
import nth.lib.common.utils.deserializer.JsonParsingFeedBack;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@ControllerAdvice
@Order(value = PriorityOrdered.HIGHEST_PRECEDENCE)
public class ValidateExceptionHandler {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationException(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new TreeMap<>();
        BindingResult bindingResult = ex.getBindingResult();
        Map<String, InvalidFormatException> jsonFeedBackErrors = JsonParsingFeedBack.ERRORS.get();
        ExceptionHandlerUtils.putBindingResultToRes(errors, bindingResult);
        ExceptionHandlerUtils.putBindingErrorsToMethodInvalidRes(errors, jsonFeedBackErrors);
        JsonParsingFeedBack.clearData();
        return ResponseEntity.badRequest().body(errors);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleConstraintViolationExceptions(
            ConstraintViolationException ex) {
        Map<String, String> errors = new TreeMap<>();
        Set<ConstraintViolation<?>> cvs = ex.getConstraintViolations();
        Map<String, InvalidFormatException> jsonFeedbackErrors = JsonParsingFeedBack.ERRORS.get();
        ExceptionHandlerUtils.putConstraintViolationToRes(errors, cvs);
        ExceptionHandlerUtils.putBindingErrorsToConstraintViolationRes(errors, cvs, jsonFeedbackErrors);
        JsonParsingFeedBack.clearData();
        return ResponseEntity.badRequest().body(errors);
    }
}
