package nth.lib.common.models;


import java.math.BigDecimal;


public class PaymentInfo {
    private String bankName;
    private BigDecimal amount;
    private String payDateTime;
    private String bankAccountNumber;
    private String contractCode;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPayDateTime() {
        return payDateTime;
    }

    public void setPayDateTime(String payDateTime) {
        this.payDateTime = payDateTime;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    @Override
    public String toString() {
        return "PaymentInfo{" +
                "bankName='" + bankName + '\'' +
                ", amount=" + amount +
                ", payDateTime='" + payDateTime + '\'' +
                ", bankAccountNumber='" + bankAccountNumber + '\'' +
                ", contractCode='" + contractCode + '\'' +
                '}';
    }
}
