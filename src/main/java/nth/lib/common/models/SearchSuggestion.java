package nth.lib.common.models;

import lombok.Data;

@Data
public class SearchSuggestion {

    String value;
    String searchKey;
}
