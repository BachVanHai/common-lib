package nth.lib.common.models;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class CommonErrorLog {
    String contractCode;
    Instant logTime;
    String errorMessage;
}
