package nth.lib.common.models;

import java.io.Serializable;

public class MailConfigDTO implements Serializable {

    private Long id;

    private String appId;

    private String host;

    private String email;

    private String password;

    private Integer port;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MailConfigDTO)) {
            return false;
        }

        return id != null && id.equals(((MailConfigDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MailConfigDTO{" +
            "id=" + getId() +
            ", appId='" + getAppId() + "'" +
            ", host='" + getHost() + "'" +
            ", email='" + getEmail() + "'" +
            ", password='" + getPassword() + "'" +
            ", port=" + getPort() +
            ", name='" + getName() + "'" +
            "}";
    }
}
