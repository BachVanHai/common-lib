package nth.lib.common.utils;
import org.owasp.encoder.Encode;



public class XSSFilter {

    public static String stripXSS(String value) {
        String cleanValue = null;
        if (value != null) {
            cleanValue = Encode.forHtml(value);
        }
        return cleanValue;
    }
}
