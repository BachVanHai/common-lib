package nth.lib.common.utils;

import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateTimeUtils {
    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    private static Instant getInstantNow(){
        int hour = Instant.now().plus(1, ChronoUnit.HOURS).atOffset(ZoneOffset.ofHours(7)).getHour();
        int minute=Instant.now().get(ChronoField.MINUTE_OF_HOUR);
        int second=Instant.now().get(ChronoField.SECOND_OF_MINUTE);
        return  Instant.now().atZone(ZoneOffset.ofHours(0)).withHour(hour).withMinute(minute).withSecond(second).withNano(0).toInstant();
    }
}
