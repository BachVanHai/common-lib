package nth.lib.common.utils.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import java.io.IOException;
import java.util.*;

public class JsonParsingFeedBack {
    public static final ThreadLocal<Map<String, InvalidFormatException>> ERRORS = ThreadLocal.withInitial(HashMap::new);
    public static final ThreadLocal<List<String>> PARSE_ERROR_VALUE = ThreadLocal.withInitial(ArrayList::new);
    public static final ThreadLocal<List<String>> REQUEST_BODY_NAME = ThreadLocal.withInitial(ArrayList::new);

    public static void gatherBindingErrors(JsonParser p, DeserializationContext ctx, InvalidFormatException e) throws IOException {
        String parent = computeParentPath(ctx.getParser().getParsingContext(), new StringBuilder()).toString();
        ERRORS.get().put(parent + p.getCurrentName(), e);
    }

    public static StringBuilder computeParentPath(JsonStreamContext parserCtx, StringBuilder sb) {
        JsonStreamContext parentCtx = parserCtx.getParent();
        if (parentCtx.getCurrentName() == null && parentCtx.getParent() == null) {
            return sb;
        }
        if (parentCtx.getCurrentName() == null) {
            Class<?> clazz = parentCtx.getCurrentValue().getClass();
            Class<?> set = null;
            try {
                set = Class.forName("java.util.Set");
            } catch (ClassNotFoundException ignored) {}
            assert set != null;
            String value = (set.isAssignableFrom(clazz)) ? "[]." :
                    String.format("[%s].", parentCtx.getCurrentIndex());
            sb.insert(0, value);
            return computeParentPath(parentCtx, sb);
        }
        String appendValue = (sb.length() == 0) ? parentCtx.getCurrentName() + "." :
                                                  (sb.charAt(0) == '[') ? parentCtx.getCurrentName() :
                                                                          parentCtx.getCurrentName() + ".";
        sb.insert(0, appendValue);
        return computeParentPath(parentCtx, sb);
    }

    public static void clearParseErrorValue() {
        PARSE_ERROR_VALUE.remove();
    }

    public static void clearData() {
        ERRORS.remove();
        PARSE_ERROR_VALUE.remove();
        REQUEST_BODY_NAME.remove();
    }
}
