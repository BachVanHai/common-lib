package nth.lib.common.utils.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.SneakyThrows;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Formatter;

public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    public LocalDate parse(JsonParser p) throws IOException {
        String value = p.getText();
        LocalDate date = null;
        try {
            date = LocalDate.parse(value, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } catch (DateTimeParseException ignored) {}
        if (date == null) {
            String format = "invalid date format (%s)";
            throw new InvalidFormatException(
                    p,
                    String.format(format, DateTimeFormatter.ISO_LOCAL_DATE.toString()),
                    value,
                    LocalDate.class);
        }
        return date;
    }

    @SneakyThrows
    @Override
    public LocalDate deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
        try {
            return parse(p);
        } catch (InvalidFormatException e) {
            JsonParsingFeedBack.gatherBindingErrors(p, ctx, e);
        }
        return null;
    }
}
