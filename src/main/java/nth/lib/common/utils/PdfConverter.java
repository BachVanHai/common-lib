package nth.lib.common.utils;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;


public class PdfConverter {

    private static String API_CONVERT_URL = "https://pdf-converter.inon.vn/forms/libreoffice/convert";

    public static byte[] convertToPdf(File convertFile) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();


        body.add("file", new FileSystemResource(convertFile));

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<byte[]> response = restTemplate.exchange(API_CONVERT_URL, HttpMethod.POST, requestEntity, byte[].class);
        return response.getBody();
    }
}
