package nth.lib.common.utils;


import nth.lib.common.models.PaymentInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseUtils {

    private static final String TP_BANK_MARK = "TPBank";
    private static final String VCB_BANK_MARK = "1010335588";

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseUtils.class);

    public static String hmacEncrypt(String text, String secret) {
        Mac sha256_HMAC;
        String algorithm = "HmacSHA256";
        try {
            sha256_HMAC = Mac.getInstance(algorithm);
            sha256_HMAC.init(new SecretKeySpec(secret.getBytes(), algorithm));
            byte[] result = sha256_HMAC.doFinal(text.getBytes());
            return hex(result);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String aesDecrypt(String encodeText, String secret) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, getKey(secret));
            return new String(cipher.doFinal(Base64.getDecoder().decode(encodeText)));
        } catch (Exception e) {
            return "";
        }
    }

    private static SecretKeySpec getKey(String myKey) {
        MessageDigest sha = null;
        SecretKeySpec secretKey;
        try {
            byte[] key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
            return secretKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String sha256Encrypt(String encodeText) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(encodeText.getBytes(StandardCharsets.UTF_8));
        return hex(hash);
    }

    public static String hex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte aByte : bytes) {
            result.append(String.format("%02x", aByte));
            // upper case
            // result.append(String.format("%02X", aByte));
        }
        return result.toString();
    }

    public static LocalDateTime getInsuranceMinStartValueDate() {
        LocalDateTime minStartValueDate = LocalDateTime.now(ZoneId.systemDefault()).withMinute(59).withSecond(59);
        int currentTime = minStartValueDate.getHour();
        if (currentTime > 0 && currentTime <= 8) {
            minStartValueDate = minStartValueDate.withHour(11);
        } else if (currentTime > 8 && currentTime <= 12) {
            minStartValueDate = minStartValueDate.withHour(17);
        } else {
            minStartValueDate = minStartValueDate.plusDays(1).withHour(11);
        }
        return minStartValueDate;
    }

    public static void setTimeout(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<>();
        for (String param : params) {
            String[] p = param.split("=");
            String name = p[0];
            if (p.length > 1) {
                String value = p[1];
                map.put(name, value);
            }
        }
        return map;
    }

    public static Optional<PaymentInfo> extractPaymentInfo(String message, String contractType) {
        try {
            int contractCodeLength = contractType.equals("CC") || contractType.equals("MC") ? 14 : 12;
            Pattern datePattern;
            Pattern amountPattern;
            Pattern contentPattern;
            Pattern bankAccountNumPattern;
            String bankName;
            String bankAccountNumber = "";
            String content = "";
            Matcher matcher;

            message += "|";

            if (message.contains(TP_BANK_MARK)) {
                bankName = "TPBank";
                datePattern = Pattern.compile(": (.*?)TK");
                bankAccountNumPattern = Pattern.compile("TK: (.*?)PS");
                amountPattern = Pattern.compile("PS:\\+(.*?)VND");
                contentPattern = Pattern.compile("ND: (.*?)\\|");
                matcher = bankAccountNumPattern.matcher(message);
                if (matcher.find()) {
                    bankAccountNumber = matcher.group(1);
                }
            } else if (message.contains(VCB_BANK_MARK)) {
                bankName = "VietcomBank";
                bankAccountNumber = VCB_BANK_MARK;
                datePattern = Pattern.compile(" luc (.*?)\\.");
                amountPattern = Pattern.compile(" \\+(.*?)VND");
                contentPattern = Pattern.compile("Ref (.*?)\\|");
            } else {
                return Optional.empty();
            }
            PaymentInfo paymentInfo = new PaymentInfo();
            paymentInfo.setBankName(bankName);
            paymentInfo.setBankAccountNumber(bankAccountNumber);
            matcher = datePattern.matcher(message);

            if (matcher.find()) {
                paymentInfo.setPayDateTime(matcher.group(1));
            }
            matcher = amountPattern.matcher(message);
            if (matcher.find()) {
                String amount =  matcher.group(1).replaceAll(",", "").replaceAll("\\.", "");
                paymentInfo.setAmount(BigDecimal.valueOf(Long.parseLong(amount)));
            }
            matcher = contentPattern.matcher(message);
            if (matcher.find()) {
                content = matcher.group(1);
            }
            int indexContractCode = content.indexOf(contractType);
            String contractCode = content.substring(indexContractCode, indexContractCode + contractCodeLength);
            paymentInfo.setContractCode(contractCode);
            return Optional.of(paymentInfo);
        } catch (Exception e) {
            return Optional.empty();
        }

    }

    public static Map<String, String> extractQueryParams(String url) {
        Map<String, String> queryParams = new HashMap<>();

        try {
            URI uri = new URI(url);
            String query = uri.getQuery();

            if (query != null && !query.isEmpty()) {
                String[] params = query.split("&");

                for (String param : params) {
                    String[] keyValue = param.split("=");

                    if (keyValue.length == 2) {
                        String key = URLDecoder.decode(keyValue[0], String.valueOf(StandardCharsets.UTF_8));
                        String value = URLDecoder.decode(keyValue[1], String.valueOf(StandardCharsets.UTF_8));
                        queryParams.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return queryParams;
    }


}
