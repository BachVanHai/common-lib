package nth.lib.common.utils;

public class ValidateMessage {
    public static final String NOT_NULL = "must be not null";
    public static final String NOT_BLANK = "must not be left blank";
    public static final String BOOLEAN = "must be boolean value (true or false)";
    public static final String NUMBER = "must be number";
    public static final String EMAIL = "must be email format";
    public static final String HOUSE_TYPE = "invalid house type (APARTMENT, VILLA, OR, TOWNHOUSE, OTHER)";
    public static final String HOUSE_OWNER_TYPE = "invalid house owner type (OWNER, OTHER)";
    public static final String IC_TYPE = "invalid ic type (CMND, CCCD, HOCHIEU, MST, KHAC)";
    public static final String CUSTOMER_TYPE = "invalid customer type (ORG, INV, BANK)";
    public static final String GENDER = "invalid gender (MALE, FEMALE, OTHER)";
    public static final String DATE_DD_MM_YYYY = "invalid date format (dd-MM-yyyy, dd/MM/yyyy, dd.MM.yyyy)";
    public static final String SPECIAL_CHAR_INVALID = "must not contain special character";
    public static final String SIZE_EXCEED = "max size is";
    public static final String PHONE_NUMBER = "invalid phone number format";
    public static final String INVALID_ADDRESS = "address contain only number, character, ',', '-' and space";
    public static final String CONTRACT_TYPE = "invalid CONTRACT_TYPE type (CC, MC, HF, TR, FD, ES)";
}
