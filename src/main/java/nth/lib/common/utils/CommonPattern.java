package nth.lib.common.utils;

public class CommonPattern {
    public static final String NUMBER = "\\d*";
    public static final String BOOLEAN = "((^true$)|(^false$))";
    public static final String CHARACTER = "^[a-zA-Z0-9]*$";
    public static final String EMAIL = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
    public static final String PHONE_NUMBER = "^0((2\\d{1,2})|3|5|7|8|9)\\d{8}$";
    public static final String VN_CHAR_WITHOUT_SPECIAL_CHAR = "^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s\\d+]+$";
    public static final String HOUSE_TYPE = "(^APARTMENT$)|(^VILLA$)|(^OR$)|(^TOWNHOUSE$)|(^OTHER$)";
    public static final String HOUSE_OWNER_TYPE = "(^OWNER$)|(^OTHER$)";
    public static final String IC_TYPE = "(^CMND$)|(^CCCD$)|(^HOCHIEU$)|(^MST$)|(^KHAC$)";
    public static final String CUSTOMER_TYPE = "(^ORG$)|(^INV$)|(^BANK$)";
    public static final String GENDER = "(^MALE$)|(^FEMALE$)|(^OTHER$)";
    public static final String DATE_DD_MM_YYYY = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
    public static final String ADDRESS = "^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s\\d+,-]+$";
}

