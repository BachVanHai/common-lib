package nth.lib.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;
import nth.lib.common.type.Constants;
import nth.lib.common.type.CurrencyType;

public class NumerFormatter {

  public static String formatAmount(String amountValue) {
    return formatAmount(amountValue, Constants.NUMBER_FORMAT_PATTERN);
  }

  public static String formatAmount(String amountValue, String pattern) {
    DecimalFormat decimalFormat = new DecimalFormat(pattern);
    String formatValue = decimalFormat.format(Objects.isNull(amountValue) ? 0 : new BigDecimal(amountValue));
    return String.format("%s %s", formatValue, CurrencyType.VND);
  }

}
