package nth.lib.common.utils;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import nth.lib.common.utils.deserializer.JsonParsingFeedBack;
import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ExceptionHandlerUtils {
    public static String getPrefix(Set<ConstraintViolation<?>> cvs) {
        Optional<ConstraintViolation<?>> optCV = cvs.stream().findFirst();
        if (optCV.isPresent()) {
            ConstraintViolation<?> cv = optCV.get();
            String[] allFields = cv.getPropertyPath().toString().split("\\.");
            return allFields[1].substring(0, allFields[1].length() - 3);
        }
        return null;
    }

    public static boolean isConstraintViolationOfList(Set<ConstraintViolation<?>> cvs) {
        Optional<ConstraintViolation<?>> optCV = cvs.stream().findFirst();
        if (optCV.isPresent()) {
            ConstraintViolation<?> cv = optCV.get();
            String root = (String) Arrays.stream(cv.getPropertyPath().toString().split("\\.")).toArray()[1];
            return root.contains("]");
        }
        return false;
    }

    public static void putConstraintViolationToRes(Map<String, String> errors, Set<ConstraintViolation<?>> cvs) {
        cvs.forEach(c -> {
            String fullFieldName = c.getPropertyPath().toString();
            String root = (String) Arrays.stream(fullFieldName.split("\\.")).toArray()[1];
            int skipItem = root.contains("]") ? 1 : 2;
            String fields = Arrays.stream(fullFieldName.split("\\."))
                                                       .skip(skipItem)
                                                       .collect(Collectors.joining("."));
            String mess = c.getMessage();
            errors.put(String.join(".", fields), mess);
        });
    }

    public static void putBindingErrorsToConstraintViolationRes(Map<String, String> errors, Set<ConstraintViolation<?>> cvs, Map<String, InvalidFormatException> jsonFeedback) {
        String prefix = getPrefix(cvs);
        jsonFeedback.forEach((k, v) -> {
            String value = isConstraintViolationOfList(cvs) ? prefix + k : k;
            errors.put(value, v.getOriginalMessage());
        });
    }

    public static void putBindingResultToRes(Map<String, String> errorsRes, BindingResult bindingResult) {
        bindingResult.getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errorsRes.put(fieldName, errorMessage);
        });
    }

    public static void putBindingErrorsToMethodInvalidRes(Map<String, String> errorsRes, Map<String, InvalidFormatException> errors) {
        List<String> prefixCtx = JsonParsingFeedBack.REQUEST_BODY_NAME.get();
        String prefix = (prefixCtx.size() > 0) ? prefixCtx.get(0) : "";
        errors.keySet().forEach(k -> {
            boolean isListRequestBody = k.charAt(0) == '[';
            String value = isListRequestBody ? prefix + k : k;
            errorsRes.put(value, errors.get(k).getOriginalMessage());
        });
    }

    public static void throwExceptionIfHasValidateErrors(BindingResult bindingResult, String paramName, int paramIndex, Class<?> clazz, String methodName) throws MethodArgumentNotValidException {
        JsonParsingFeedBack.REQUEST_BODY_NAME.get().add(paramName);
        if (bindingResult.hasErrors() || JsonParsingFeedBack.ERRORS.get().size() > 0) {
            Method[] methods = clazz.getDeclaredMethods();
            int index = IntStream.range(0, methods.length - 1)
                    .filter(i -> methods[i].getName().equals(methodName))
                    .findFirst().orElse(-1);
            throw new MethodArgumentNotValidException(new MethodParameter(methods[index], paramIndex), bindingResult);
        }
    }
}
