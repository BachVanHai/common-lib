package nth.lib.common.mail;

import nth.lib.common.models.MailConfigDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Service
public class MailService {
    private static final String CONTENT_TYPE_TEXT_HTML = "text/html;charset=\"utf-8\"";
    private final ThymeleafService thymeleafService;

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    public MailService(ThymeleafService thymeleafService) {

        this.thymeleafService = thymeleafService;
    }

    @Async
    public void sendMailAsync(MailConfigDTO mailConfig, String templateName, String subject, String receiverEmail, List<EmailVariable> emailVariables) {
        this.sendMail(mailConfig, templateName, subject, receiverEmail, emailVariables);
    }

    @Async
    public void sendMailAsync(Message mimeMessage) {
        this.sendMail(mimeMessage);
    }

    private void sendMail(Message mimeMessage) {
        try {
            Transport.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void sendMail(MailConfigDTO mailConfig, String templateName, String subject, String receiverEmail, List<EmailVariable> emailVariables) {
        Properties props = new Properties();
        props.put("mail.smtp.host", mailConfig.getHost());
        props.put("mail.smtp.port", mailConfig.getPort() != null ? mailConfig.getPort() : "25");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(mailConfig.getEmail(), mailConfig.getPassword());
                    }
                });
        MimeMessage message = new MimeMessage(session);
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, "UTF-8");
        try {
            messageHelper.setTo(receiverEmail);
            messageHelper.setFrom(String.format("%s <%s>", mailConfig.getName(), mailConfig.getEmail()));
            messageHelper.setSubject(subject);
            messageHelper.setText(thymeleafService.getContent(templateName, emailVariables), true);
            Transport.send(messageHelper.getMimeMessage());
        } catch (MessagingException e) {
            log.info(e.getMessage() + "-" + receiverEmail);
        }
    }

    public Builder getBuilder() {
        return new Builder(thymeleafService);
    }

    public static class Builder {
        final Logger log = LoggerFactory.getLogger(Builder.class);
        List<EmailVariable> emailVariables = new ArrayList<>();
        List<EmailFile> emailFileAttachements = new ArrayList<>();
        List<EmailResource> emailResourceAttachements = new ArrayList<>();
        List<EmailFile> emailFileIlines = new ArrayList<>();
        List<EmailResource> emailResourceInlines = new ArrayList<>();
        private String templateName;
        private String subject;
        private String receiver;
        private String[] emailCC;
        private MailConfigDTO mailConfig;
        private final ThymeleafService thymeleafService;
        public Builder(ThymeleafService thymeleafService) {
            this.thymeleafService = thymeleafService;
        }

        public String[] getEmailCC() {
            return emailCC;
        }

        public void setEmailCC(String[] emailCC) {
            this.emailCC = emailCC;
        }

        public Builder setMailConfig(MailConfigDTO mailConfig) {
            this.mailConfig = mailConfig;
            return this;
        }

        public Builder setTemplateName(String templateName) {
            this.templateName = templateName;
            return this;
        }

        public Builder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder setReceiver(String receiver) {
            this.receiver = receiver;
            return this;
        }

        public Builder addVariable(EmailVariable emailVariable) {
            this.emailVariables.add(emailVariable);
            return this;
        }

        public Builder addVariables(List<EmailVariable> emailVariables) {
            this.emailVariables.addAll(emailVariables);
            return this;
        }

        public Builder addEmailFileAttachements(List<EmailFile> emailFileAttachements) {
            this.emailFileAttachements.addAll(emailFileAttachements);
            return this;
        }

        public Builder addEmailResourceAttachements(List<EmailResource> emailResourceAttachements) {
            this.emailResourceAttachements.addAll(emailResourceAttachements);
            return this;
        }

        public Builder addEmailFileIlines(List<EmailFile> emailFileIlines) {
            this.emailFileIlines.addAll(emailFileIlines);
            return this;
        }

        public Builder addEmailResourceInlines(List<EmailResource> emailResourceInlines) {
            this.emailResourceInlines.addAll(emailResourceInlines);
            return this;
        }

        public Builder addEmailFileAttachement(EmailFile emailFileAttachement) {
            this.emailFileAttachements.add(emailFileAttachement);
            return this;
        }

        public Builder addEmailResourceAttachement(EmailResource emailResourceAttachement) {
            this.emailResourceAttachements.add(emailResourceAttachement);
            return this;
        }

        public Builder addEmailFileIline(EmailFile emailFileIline) {
            this.emailFileIlines.add(emailFileIline);
            return this;
        }

        public Builder addEmailResourceInline(EmailResource emailResourceInline) {
            this.emailResourceInlines.add(emailResourceInline);
            return this;
        }


        public Message build() throws MessagingException {
            Properties props = new Properties();
            props.put("mail.smtp.host", mailConfig.getHost());
            props.put("mail.smtp.port", mailConfig.getPort());
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
//            props.put("mail.debug", "true");

            Session session = Session.getInstance(props,
                    new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(mailConfig.getEmail(), mailConfig.getPassword());
                        }
                    });
            Message message = new MimeMessage(session);
            MimeMessageHelper helper = new MimeMessageHelper((MimeMessage) message, MimeMessageHelper.MULTIPART_MODE_MIXED, "UTF-8");
            helper.setText(thymeleafService.getContent(templateName, emailVariables), true);
            this.emailFileIlines.forEach(emailInlineOrAttach -> {
                try {
                    helper.addInline(emailInlineOrAttach.getName(), emailInlineOrAttach.getValue());
                } catch (MessagingException e) {
                    log.info(e.getMessage());
                }
            });

            this.emailResourceInlines.forEach(emailInlineOrAttach -> {
                try {
                    helper.addInline(emailInlineOrAttach.getName(), emailInlineOrAttach.getValue(), emailInlineOrAttach.contentType);
                } catch (MessagingException e) {
                    log.info(e.getMessage());
                }
            });

            this.emailFileAttachements.forEach(emailResourceAttachement -> {
                try {
                    helper.addAttachment(emailResourceAttachement.name, emailResourceAttachement.value);
                } catch (MessagingException e) {
                    log.info(e.getMessage());
                }
            });
            this.emailResourceAttachements.forEach(emailResourceAttachement -> {
                try {
                    helper.addAttachment(emailResourceAttachement.name, emailResourceAttachement.value);
                } catch (MessagingException e) {
                    log.info(e.getMessage());
                }
            });
            helper.setSubject(subject);
            helper.setFrom(String.format("%s <%s>", mailConfig.getName(), mailConfig.getEmail()));
            helper.setTo(InternetAddress.parse(receiver));
            if (emailCC != null) {
                helper.setCc(emailCC);
            }
//            message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(receiver)});
//            message.setFrom(new InternetAddress(email));
//            message.setSubject(subject);
//            message.setContent(thymeleafService.getContent(templateName, emailVariables), CONTENT_TYPE_TEXT_HTML);
            return helper.getMimeMessage();
        }
    }
}
