package nth.lib.common.mail;

import lombok.Data;
import org.springframework.core.io.Resource;

import java.io.File;

@Data
public class EmailResource {
    String name;
    Resource value ;
    String contentType;

    public EmailResource(String name, Resource value, String contentType) {
        this.name = name;
        this.value = value;
        this.contentType = contentType;
    }
}
