package nth.lib.common.mail;

import lombok.Data;

import java.io.File;

@Data
public class EmailFile{
    String name;
    File value ;

    public EmailFile(String name, File value) {
        this.name = name;
        this.value = value;
    }
}
