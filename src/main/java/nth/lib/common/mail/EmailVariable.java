package nth.lib.common.mail;

import lombok.Data;

@Data
public class EmailVariable {
    String name;
    Object value;

    public EmailVariable(String name, Object value) {
        this.name = name;
        this.value = value;
    }
}
