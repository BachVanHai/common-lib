package nth.lib.common.security;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import nth.lib.common.type.CurrencyType;
import nth.lib.common.type.Constants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IPHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(IPHelper.class);

    private final static String[] HEADERS_TO_TRY = {
            "X-Forwarded-For",
            "X-Forwared-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"
    };

    public static String getClientIpAddress(HttpServletRequest request) {
        try {
            LOGGER.info("----------- HTTP_SERVLET_REQUEST_HEADER -------------");

            Collections.list(request.getHeaderNames()).forEach(e -> LOGGER.info(e + " ---> " + request.getHeader(e)));

            for (String header : HEADERS_TO_TRY) {
                String ip = request.getHeader(header);
                if (StringUtils.isNotEmpty(ip) && !"unknown".equalsIgnoreCase(ip)) {
                    return StringUtils.trim(ip);
                }
            }

            LOGGER.info("REMOTE ADDRESS ---> " + request.getRemoteAddr());
            return StringUtils.trim(request.getRemoteAddr());
        } catch (Exception e) {
            LOGGER.warn("Get Client Address error", e);
        }

        return null;
    }

    public static String getClientIpAddressByIndex(HttpServletRequest request, int index) {
        String ipClientAddress = getClientIpAddress(request);
        if (StringUtils.isBlank(ipClientAddress)) return null;

        String[] listAddress = StringUtils.split(ipClientAddress, ",");
        if (listAddress.length < index) {
            return null;
        }

        return StringUtils.trim(listAddress[index]);
    }

    public static String getFirstClientIpAddress(HttpServletRequest request) {
        return getClientIpAddressByIndex(request, 0);
    }

}
